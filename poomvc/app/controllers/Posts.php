<?php

use Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{

    private $postModel;
    private $userModel;

    public function __construct()
    {
        //Protege la vista posts mediante la función isLoggedIn
        // Para ello comprueba en el constructor de Posts, con la función isLoggedIn, si un usuario no está  logueado
        if (!isLoggedIn()) {
            urlRedirect('/users/login');
        }

        $this->postModel = $this->model('Post');
        $this->userModel = $this->model('User');
    }

    // método index que cargue la vista index de post pasándole el array $data.

    public function index()
    {

        $posts = $this->postModel->getPosts();

        $data = [
            'titulo' => 'Index de posts',
            'posts' => $posts
        ];

        return $this->view('posts/index', $data);
    }


    public function add()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $flag_err = true;

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['id'],
                'title_err' => '',
                'body_err' => ''
            ];



            if (empty($data['title'])) {

                $data['title_err'] = 'Tienes que ponerle un título';
                $flag_err = false;
            } else {
                $data['title_err'] = '';
            }

            if (empty($data['body'])) {
                $data['body_err'] = 'Tienes que introducir algún dato';
                $flag_err = false;
            } else {

                $data['body_err'] = '';
            }


            // Si los campos de error están vacíos, insertamos. 
            if ($flag_err) {


                $this->postModel->addPost($data);
                $flash = new Flash();
                $flash->message('El Post se ha creado.');
                urlRedirect('/posts/index');
            } else {
                // Si hay algún error
                $this->view('posts/add', $data);
            }
        } else {

            $data = [
                'title' => '',
                'body' => '',
                'user_id' => '',
                'title_err' => '',
                'body_err' => ''
            ];

            return $this->view('posts/add', $data);
        }
    }

    public function show($id)
    {
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($post->user_id);

        $data = [
            'user' => $user,
            'post' => $post
        ];

        return $this->view('posts/show', $data);
    }


    public function edit($id)
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);


            $data = [
                'id' => $id,
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['id'],
                'title_err' => '',
                'body_err' => ''
            ];

            // comprobamos si los campos están vacios
            $flag_err = true;
            if (empty($data['title'])) {
                $data['title_err'] = 'Tienes que poner el título';
                $flag_err = false;
            } else {
                $data['title_err'] = '';
            }

            if (empty($data['body'])) {
                $data['body_err'] = 'Tienes que poner algo en el post';
                $flag_err = false;
            } else {
                $data['body_err'] = '';
            }

            // Si los campos de error están vacíos, insertamos. 
            if ($flag_err) {
                $this->postModel->updatePost($data);
                $flash = new Flash();
                $flash->message('El post se actualizó');
                urlRedirect('/posts/index');
            } else {
                // mostramos errores
                $this->view('posts/edit', $data);
            }
        } else {

            $post = $this->postModel->getPostById($id);

            if ($post->user_id !== $_SESSION['id']) {
                $flash = new Flash();
                $flash->error('No tienes permisos para editar este post');
                urlRedirect('/posts/index');
            }

            $data = [
                'id' => $id,
                'title' => $post->title,
                'body' => $post->body,
                'user_id' => $post->user_id,
            ];

            return $this->view('posts/edit', $data);
        }
    }

    public function delete($id){
        

        if( $_SERVER['REQUEST_METHOD'] == 'POST' ){

            $post = $this->postModel->getPostById($id);

            if ($post->user_id !== $_SESSION['id']) {
                $flash = new Flash();
                $flash->error('Necesitas permisos para eliminar el post');
                urlRedirect('/posts/index');
            }else{

                
                    $this->postModel->deletePost($post->id);
                    $flash = new Flash();
                    $flash->message('Se ha eliminado el posts');
                    urlRedirect('/posts/index');
                  }
        }else{
            
            $flash = new Flash();
            $flash->error('Necesitas permisos para eliminar el post');
            urlRedirect('/posts/index');
        }
    }



}
