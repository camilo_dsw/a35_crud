<?php require_once APPROOT . '/views/partials/header.php'; ?>
<a class="btn btn-warning pull-right" href="<?=URLROOT . '/posts/index'  ?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<div class="card card-body bg-light mt-5" style="width:40rem;">
    <h2>Crear publicación</h2>
    <p>Por favor introduzca los datos de su publicación</p>
        <form method="POST" action="<?= URLROOT . '/posts/add' ?>">
        <div class="form-group">
            <label for="title">Título: <sup>*</sup></label>
            <input type="text" name="title" class="form-control <?= empty($data['title_err']) ? '' : 'is-invalid' ?>" 
            placeholder="Título de la publicación" value="<?= isset($data['title']) ? $data['title'] : ''?>">
            <span class="invalid-feedback">
                <?= isset($data['title_err']) ? $data['title_err'] : '' ?>
            </span>
        </div>
        <div class="form-group" style="width: 35rem;">
            <label for="body">Contenido: <sup>*</sup></label>
            <textarea name="body" class="form-control <?= empty($data['body_err']) ? '' : 'is-invalid' ?> " rows="5" 
            placeholder="Su contenido">

            </textarea>

            <span class="invalid-feedback">
                <?= isset($data['body_err']) ? $data['body_err'] : '' ?>
            </span>
        </div>
        <div class="row">
            <div class="col">
                <input type="submit" value="Crear publicación" class="btn btn-primary btn-block">
            </div>
        </div>
    </form>
</div>
<?php require_once APPROOT . '/views/partials/footer.php'; ?>
